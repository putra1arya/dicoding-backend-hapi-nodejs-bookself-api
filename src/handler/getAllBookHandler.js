const books = require('../books')
const getAllBookHandler = (request, h) => {
  const { reading, name, finished } = request.query
  if (reading !== undefined) {
    const newreading = parseInt(reading) === 1
    const filteredBooks = books.filter(book => book.reading === newreading)
    const newFilteredBooks = filteredBooks.map(book => {
      const { id, name, publisher } = book
      const temp = { id, name, publisher }
      return temp
    })
    const response = h.response({
      status: 'success',
      data: {
        books: newFilteredBooks
      }
    })
    response.code(200)
    return response
  }
  if (finished !== undefined) {
    const newfinished = parseInt(finished) === 1
    const filteredBooks = books.filter(book => book.finished === newfinished)
    const newFilteredBooks = filteredBooks.map(book => {
      const { id, name, publisher } = book
      const temp = { id, name, publisher }
      return temp
    })
    const response = h.response({
      status: 'success',
      data: {
        books: newFilteredBooks
      }
    })
    response.code(200)
    return response
  }
  if (name !== undefined) {
    const filteredBooks = books.filter(book => book.name.toLowerCase().match(name.toLowerCase()))
    const newFilteredBooks = filteredBooks.map(book => {
      const { id, name, publisher } = book
      const temp = { id, name, publisher }
      return temp
    })
    const response = h.response({
      status: 'success',
      data: {
        books: newFilteredBooks
      }
    })
    response.code(200)
    return response
  }
  const newBook = books.map(book => {
    const { id, name, publisher } = book
    const temp = { id, name, publisher }
    return temp
  })
  const response = h.response({
    status: 'success',
    data: {
      books: newBook
    }
  })
  response.code(200)
  return response
}
module.exports = getAllBookHandler
