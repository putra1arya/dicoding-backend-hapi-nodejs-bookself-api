const books = require('../books')
const getDetailBookHandler = (request, h) => {
  const { bookId } = request.params
  const myBook = books.find(book => bookId === book.id)
  if (myBook === undefined) {
    const response = h.response({
      status: 'fail',
      message: 'Buku tidak ditemukan'
    })
    response.code(404)
    return response
  }
  const response = h.response({
    status: 'success',
    data: {
      book: myBook
    }
  })
  response.code(200)
  return response
}

module.exports = getDetailBookHandler
