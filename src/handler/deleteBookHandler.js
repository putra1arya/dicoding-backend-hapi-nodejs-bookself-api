const books = require('../books')
const deleteBookHandler = (request, h) => {
  const { bookId } = request.params
  const myBook = books.findIndex(book => book.id === bookId)
  if (myBook < 0) {
    const response = h.response({
      status: 'fail',
      message: 'Buku gagal dihapus. Id tidak ditemukan'
    })
    response.code(404)
    return response
  }
  books.splice(myBook, 1)
  const response = h.response({
    status: 'success',
    message: 'Buku berhasil dihapus'
  })
  response.code(200)
  return response
}
module.exports = deleteBookHandler
