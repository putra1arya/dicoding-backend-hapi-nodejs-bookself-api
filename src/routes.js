const addBookHandler = require('./handler/addBookHandler')
const getAllBookHandler = require('./handler/getAllBookHandler')
const getDetailBookHandler = require('./handler/getDetailBookHandler')
const editBookHandler = require('./handler/editBookHandler')
const deleteBookHandler = require('./handler/deleteBookHandler')
const routes = [
  {
    path: '/books',
    method: 'POST',
    handler: addBookHandler
  },
  {
    path: '/books',
    method: 'GET',
    handler: getAllBookHandler
  },
  {
    path: '/books/{bookId}',
    method: 'GET',
    handler: getDetailBookHandler
  },
  {
    path: '/books/{bookId}',
    method: 'PUT',
    handler: editBookHandler
  },
  {
    path: '/books/{bookId}',
    method: 'DELETE',
    handler: deleteBookHandler
  }
]

module.exports = routes
